variable "aws_region" {
  description = "The AWS region to deploy resources in"
  type        = string
  default     = "us-east-1"
}

variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  description = "The CIDR block for the subnet"
  type        = string
  default     = "10.0.1.0/24"
}

variable "availability_zone" {
  description = "The availability zone for the subnet"
  type        = string
  default     = "us-east-1a"
}

variable "my_ip" {
  description = "its 4 my ip enable all traffic"
  type        = string
}

variable "sg_name" {
  description = "Name of the sg"
  type        = string
  default     = "test"
}

variable "sg_description" {
  description = "Description of the sg"
  type        = string
  default     = "Allow SSH and HTTP from my IP"
}

variable "ami_id" {
  description = "AMI ID for the EC2"
  type        = string
  default     = "ami-0c55b159cbfafe1f0" # Ubuntu 20.04 LTS Free Tier
}

variable "instance_type" {
  description = "EC2 instance type"
  type        = string
  default     = "t2.micro"
}

#SSM policy attach, was manual created TODO list
variable "iam_instance_profile_arn" {
  description = "The ARN of IAM instance profile to attach t instance"
  type        = string
}

variable "key_name" {
  description = "SSH access key name"
  type        = string
}

variable "user_data_file" {
  description = "path to bootstrap"
  type        = string
  default     = "bootstrap.sh"
}

variable "default_tags" {
  description = "Default tags applied to all resources"
  type        = map(string)
  default     = {
    Environment = "test"
    Project     = "drumncode"
  }
}

variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
  default     = "test"
}

variable "subnet_name" {
  description = "Name of the Subnet"
  type        = string
  default     = "test"
}

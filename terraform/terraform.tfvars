# AWS region to deploy in
aws_region = "us-east-1"

# The CIDR block for the VPC
vpc_cidr_block = "10.0.0.0/16"

# The CIDR block for the subnet
subnet_cidr_block = "10.0.1.0/24"

# The availability zone for the subnet
availability_zone = "us-east-1a"

# IP address to allow SSH and HTTP access 
#here u can add yr personal IP for connect 
my_ip = "xxx.xxx.xxx.xxx/xx"

# Name of security group
sg_name = "test"

# Description of the sg
sg_description = "Allow SSH and HTTP"

# AMI ID for EC2 instance (Ubuntu 20.04 LTS )
ami_id = "ami-0e001c9271cf7f3b9"

# EC2 instance type
instance_type = "t2.micro"

iam_instance_profile_arn = "arn profile here"  

# Key name for SSH access
key_name = "dnc"

# Path to the userdata script
user_data_file = "bootstrap.sh"

# Default tags to be applied to all resources
default_tags = {
  Environment = "test"
  Project     = "drumncode"
}

# Name of the VPC
vpc_name = "test"

# Name of the Subnet
subnet_name = "test"


module "network" {
  source = "./modules/network"

  vpc_cidr_block    = var.vpc_cidr_block
  subnet_cidr_block = var.subnet_cidr_block
  availability_zone = var.availability_zone
  sg_name           = var.sg_name
  sg_description    = var.sg_description
  my_ip             = var.my_ip
  default_tags      = var.default_tags
  vpc_name          = var.vpc_name
  subnet_name       = var.subnet_name
}

 #####  TO DO, fix manual added iam policy 
module "iam" {
  source = "./modules/iam"
  
  default_tags = var.default_tags
}

module "server" {
  source = "./modules/server"

  ami_id                   = var.ami_id
  instance_type            = var.instance_type
  key_name                = var.key_name
  user_data_file           = var.user_data_file
  default_tags             = var.default_tags
  security_group_id        = module.network.security_group_id
  subnet_id                = module.network.subnet_id
  iam_instance_profile_name = module.iam.instance_profile_name
}

output "instance_public_ip" {
  description = "The public IP address of the instance"
  value       = module.server.instance_public_ip
}

output "instance_id" {
  description = "The ID of the instance"
  value       = module.server.instance_id
}

variable "ami_id" {
  description = "AMI ID for EC2"
  type        = string
}

variable "instance_type" {
  description = "Instance type EC2"
  type        = string
}

variable "iam_instance_profile_name" {
  description = "The name of the IAM instance profile to attach to the instance"
  type        = string
}



variable "key_name" {
  description = "Key name for SSH access"
  type        = string
}

variable "user_data_file" {
  description = "bootstarp loc"
  type        = string
}

variable "default_tags" {
  description = "Default tags to be applied to all resources"
  type        = map(string)
}

variable "security_group_id" {
  description = "Security group ID"
  type        = string
}

variable "subnet_id" {
  description = "Subnet ID"
  type        = string
}

variable "instance_name" {
  description = "Name of EC2 instance"
  type        = string
  default     = "drumncode"
}

variable "vpc_cidr_block" {
  description = "CIDR block forVPC"
  type        = string
}

variable "subnet_cidr_block" {
  description = "CIDR blokc for subnet"
  type        = string
}

variable "availability_zone" {
  description = "Availability zone for subnet"
  type        = string
}

variable "sg_name" {
  description = "Name of security grp"
  type        = string
}

variable "sg_description" {
  description = "Description of security grp"
  type        = string
}

variable "my_ip" {
  description = "IP address to allow SSH and HTTP access"
  type        = string
}

variable "default_tags" {
  description = "Default tags to be applied for all resources"
  type        = map(string)
}

variable "vpc_name" {
  description = "Name of VPC"
  type        = string
}

variable "subnet_name" {
  description = "Name of Subnet"
  type        = string
}

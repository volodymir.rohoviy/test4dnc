#####Project Documentation
Overview

This repository contains two main directories:

 
-    project: This directory houses a basic Laravel application along with Docker and Docker Compose files necessary for containerization. Additionally, it includes .yml files for GitLab CI/CD pipelines.
-  terraform: This directory contains Terraform configuration files designed to provision an EC2 instance and the associated infrastructure.

#####Directory Structure


1. project
    - app files           # Basic Laravel application code
    - docker              # Docker configuration files
    - docker-compose.yml  # Docker Compose file
    - .gitlab-ci.yml      # GitLab CI/CD pipeline configuration

2. terraform
    - main.tf             # Main Terraform configuration file
    - variables.tf        # Terraform variables file
    - bootstrap.sh        # Shell script for initial setup
    - terraform.tfvars    # Variables file for Terraform
    - modules/            # Directory containing reusable Terraform modules(IAM,EC2 and network)

Usage

    CI/CD Pipeline:
      The .gitlab-ci.yml file contains the configuration for GitLab CI/CD pipelines to build and deployment on EC2.

Terraform Infrastructure

    Initialize Terraform:

        Navigate to the terraform directory.

        Initialize the Terraform environment:

        

    terraform init

Apply Configuration:

    Apply the Terraform configuration to provision the EC2 instance and infrastructure:

    

    terraform apply

Diagrams
Current State

    Variant 1: actual state
        EC2 instance runs Docker containers for the Laravel application and gitlab-runner. EC2 instance and infrastructure provisioned using Terraform.

    Variant 2: advanced infra. Added gitlab-runner and docker hub. Build on 1 or more servers
        GitLab CI/CD pipeline automates build and deployment processes.

    Variant 3: some variant of best practise 
        watch diagram
        